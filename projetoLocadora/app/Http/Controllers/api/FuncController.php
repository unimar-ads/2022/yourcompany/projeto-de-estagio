<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FuncController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $func = Func::all();

        return response()->json($func);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'nome' => 'required|max:255',
            'RG' => 'required|max:9',
            'CPF' => 'required|max:11',
            'funcao' => 'required',
        ]);

        if ($validated->fails()) {
            return response()->json([
                'message' => 'Dados estão Incorretos!'
            ], 400);
        }

        $func = Func::create($request->all());

        return response()->json([
            'message' => 'Cadastro de Funcionário Completo!',
            'func' => $func
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Func  $func
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return Func::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Func  $func
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Func $func)
    {
        $validated = Validator::make($request->all(), [
            'CPF' => 'max:11'
        ]);

        if ($validated->fails()) {
            return response()->json([
                'message' => 'Seu cadastro está incorreto!'
            ], 400);
        }

        $func->update($request->all());

        return response()->json([
            'message' => 'Cadastro do Funcionário modificado!',
            'func' => $func
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Func  $func
     * @return \Illuminate\Http\Response
     */
    public function destroy(Func $func)
    {
        $func->delete();

        return response()->json([
            'message' => "Cadastro do Funcionário deletado!"
        ], 200);
    }
}
