<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Cars;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Cars::all();

        return response()->json($cars);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'nome' => 'required|max:255',
            'cor' => 'required',
            'motor' => 'required',
            'marca' => 'required',
            'valor' => 'required',
            'data_compra' => 'required',
        ]);

        if ($validated->fails()) {
            return response()->json([
                'message' => 'Dados estão faltando!'
            ], 400);
        }

        $cars = Cars::create($request->all());

        return response()->json([
            'message' => 'Cadastro de carro Completo!',
            'cars' => $cars
        ], 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cars  $cars
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return Cars::findOrFail($id);

        //return response()->json([
            //'message' => 'Success',
            //'cars' => $cars
        //], 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cars  $cars
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cars $cars)
    {
        $validated = Validator::make($request->all(), [
            'nome' => 'max:255'
        ]);
        
        if ($validated->fails()) {
            return response()->json([
                'message' => 'Seu cadastro está incorreto!'
            ], 400);
        }

        $cars->update($request->all());

        return response()->json([
            'message' => 'Cadastro do Carro modificado!',
            'cars' => $cars
        ], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cars  $cars
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cars $cars)
    {
        $cars->delete();

        return response()->json([
            'message' => "Cadastro do Carro deletado!"
        ], 200);

    }
}
