<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
    use HasFactory;

    protected $table = "cars";

    protected $fillable = [
        "nome",
        "cor",
        "motor",
        "marca",
        "valor",
        "data_compra",
        "data_venda"
    ];

}
